const ngModule = angular.module('TwineHealth', [
  'ui.router',
  'ngMaterial',
  'md.data.table'
]);

import appComponents from './components';
import appDirectives from './directives';
import appFactories from './factories';
import appServices from './services';

appComponents(ngModule);
appDirectives(ngModule);
appFactories(ngModule);
appServices(ngModule);

import {appConfig} from './app-config';
import {appCtrl} from './app-ctrl';
import {appRoutes} from './app-routes';
import {appRunblock} from './app-runblock';

ngModule
  .config(appConfig)
  .config(appRoutes)
  .run(appRunblock)
  .controller('AppCtrl', appCtrl);

import _ from 'lodash';
import moment from 'moment';

export default class patientsService {

  constructor /* @ngInject */ () {
    this.patients = require('./patients.json');
  }

  dtParse(dt = Date.now()) {
    return moment(dt).format('YYYY-MM-DD').split('-').map(i => parseInt(i));
  }

  _age(dob) {
    dob = this.dtParse(dob);
    let now = this.dtParse();

    let years_old = now[0] - dob[0];

    if (now[1] < dob[1]) {
      if (now[2] >= dob[2]) {
        years_old--;
      }
    }

    return years_old;
  }

  parsePatients(patients) {

    let parsed = [];

    _.forEach(patients, patient => {
      patient.age = this._age(patient['birthDate']);
      patient.birthDate = moment(patient.birthDate, 'YYYY-MM-DD').format('MM/DD/YYYY');
      parsed.push(patient);
    });

    return parsed;
  }

  get patients() {
    return this._patients;
  }

  set patients(value) {
    this._patients = this.parsePatients(value);
  }

}

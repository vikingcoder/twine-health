import patientsService from './patients.service';

export default ngModule => {

  ngModule
    .service('$patient', patientsService)

}

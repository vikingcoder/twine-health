function appRoutes /* @ngInject */($stateProvider) {

  $stateProvider
    .state('app', {
      url: '/',
      views: {
        'toolbar': {
          controller: 'toolbarCtrl',
          controllerAs: 'tb',
          templateProvider: () => require('./components/toolbar/toolbar.html')
        },
        'main': {
          controller: 'dataTableCtrl',
          controllerAs: 'dt',
          templateProvider: () => require('./components/dataTable/dataTable.html')
        }
      }
    })

}
export {appRoutes}

import ngIcon from './ngIcon/ngIcon.directive';

export default ngModule => {

  ngModule
    .directive('ngIcon', ngIcon)

}

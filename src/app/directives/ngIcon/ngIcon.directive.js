export default function ngIcon /* @ngInject */ ($interpolate, $compile) {

  let tpl = require('./ngIcon.template.html');

  return {
    restrict: 'E',
    scope: {
      icon: '@'
    },
    link: ($scope, $ele) => {
      $scope.icon = $scope.icon || $interpolate($ele.text())($scope);
      $ele.replaceWith($compile(tpl)($scope));
    }
  }

}

function appRunblock /* @ngInject */ ($rootScope) {

  // Some simple config
  $rootScope.titles = {
    toolbar: 'Twine Health Code Test',
    window: 'Twine Health Code Test | Bill Brady'
  };

  $rootScope.page = {
    toolbarTitle: $rootScope.titles.toolbar
  };

}
export {appRunblock}

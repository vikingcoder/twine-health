class appCtrl {

  constructor /* @ngInject */ ($rootScope) {

    this.$rootScope = $rootScope;

  }

  get $rootScope() {
    return this._$rootScope;
  }

  set $rootScope(value) {
    this._$rootScope = value;
  }

}
export {appCtrl}

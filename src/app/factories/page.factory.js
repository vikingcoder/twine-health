function pageFactory /* @ngInject */($rootScope, $window) {

  let $pageFactory = {};
  let separators = {
    toolbar: ' - ',
    window: ' | '
  };

  function parseTitle(title, separator, prepend = false) {

    if (title) {
      let titleTo = typeof title;

      switch (titleTo) {
        case 'string':
          title = title.trim();
          if (title == '') {
            return null;
          }
          break;
        case 'object':
          title = title.join(separator);
          break;
        default:
          return null;
      }
    } else {
      return null;
    }

    return prepend ? separator + title : title;
  }

  function _windowTitle(title, separator = separators.window) {
    title = parseTitle(title, separator);
    title = title ? title + separator : '';
    $window.document.title = title + $rootScope.titles.window;
  }

  function _toolbarTitle(title, separator = separators.toolbar) {
    title = parseTitle(title, separator, true);
    title = title || '';
    $rootScope.page.toolbarTitle = $rootScope.titles.toolbar + title;
  }

  $pageFactory.titles = (title) => {
    _windowTitle(title);
    _toolbarTitle(title);
  };

  $pageFactory.windowTitle = title => _windowTitle(title);

  $pageFactory.toolbarTitle = title => _toolbarTitle(title);

  return $pageFactory;

}
export {pageFactory}

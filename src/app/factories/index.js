import {pageFactory} from './page.factory';

export default ngModule => {

  ngModule
    .factory('$page', pageFactory)

}

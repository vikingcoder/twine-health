function appConfig /* @ngInject */ ($mdThemingProvider, $mdIconProvider, $locationProvider, $urlRouterProvider) {

  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');

  $mdThemingProvider.theme('default')
    .primaryPalette('brown')
    .accentPalette('red');
  $mdIconProvider.defaultIconSet('../assets/icons/mdi.svg');

}
export {appConfig}

import _ from 'lodash';

class dataTableCtrl {

  constructor /* @ngInject */($patient, $page, $log, $rootScope) {

    const self = this;

    this.$log = $log;
    this.$patient = $patient;

    $page.titles('Patient List');

    this.patients = $patient.patients;

    this.showFilters = false;

    this.filters = {
      minAge: 18,
      maxAge: 99,
      systolic: 140,
      diastolic: 90,
      minBp: new Date(2014, 0, 1),
      maxBp: new Date(),
      firstName: '',
      lastName: '',
      emailAddress: ''
    };

    this.originalFilters = angular.copy(this.filters);

    this.pagination = {
      order: 'age',
      options: [5],
      limit: 5,
      page: 1
    };

    $rootScope.$watchCollection(
      () => self.filters,
      (newVal, oldVal) => angular.equals(newVal, oldVal) || self.filterPatients()
    );

    this.noFilters = false;
    this.filterPatients();
  }

  /**
   * Makes the select for how many patients to view on the page adapt to next highest 5
   */
  pagOptions() {
    let rounds = Math.ceil(this.patients.length / 5);
    let options = [];

    for (let r = 5; r <= (rounds * 5); r += 5) {
      options.push(r);
    }

    this.pagination.options = options;
  }

  /**
   * Toggles the filters and refreshes the patient list
   */
  toggleFilters() {
    this.noFilters = !this.noFilters;
    this.filterPatients();
  }

  /**
   * Filters the patients by matching filter
   */
  filterPatients() {

    if (this.noFilters) {
      this.patients = this.$patient.patients;
      this.pagOptions();
      return;
    }

    let filters = this.filters;
    let patients = this.$patient.patients;
    let filtered = [];

    _.forEach(patients, patient => {
      let add = true;

      ['age', 'latestBloodPressureDate'].forEach(k => {
        let _k = k == 'age' ? 'Age' : 'Bp';
        if (!patient[k] || patient[k] <= filters['min' + _k] || patient[k] >= filters['max' + _k]) {
          add = false;
        }
      });

      if (patient.systolic < filters.systolic && patient.diastolic < filters.diastolic) {
        add = false;
      }

      ['firstName', 'lastName', 'emailAddress'].forEach(k => {
        if (filters[k].trim() !== '') {
          if (patient[k].substring(0, filters[k].length).toLowerCase() != filters[k].toLowerCase()) add = false;
        }
      });

      if (add) {
        filtered.push(patient);
      }
    });

    this.patients = filtered;
    this.pagOptions();
  }

  /**
   * Resets the filters
   */
  resetFilters() {
    this.filters = angular.copy(this.originalFilters);
    this.filterPatients();
  }

  // Getters & Setters
  get noFilters() {
    return this._noFilters;
  }

  set noFilters(value) {
    this._noFilters = value;
  }

  get $log() {
    return this._$log;
  }

  set $log(value) {
    this._$log = value;
  }

  get $patient() {
    return this._$patient;
  }

  set $patient(value) {
    this._$patient = value;
  }

}
export {dataTableCtrl}

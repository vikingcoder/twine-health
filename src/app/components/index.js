import {dataTableCtrl} from './dataTable/dataTable.ctrl';
import {toolbarCtrl} from './toolbar/toolbar.ctrl';

export default ngModule => {

  ngModule
    .controller('dataTableCtrl', dataTableCtrl)
    .controller('toolbarCtrl', toolbarCtrl)

}

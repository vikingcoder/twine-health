'use strict';

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const env = (process.env.NODE_ENV || 'development').toLowerCase();
const port = (process.env.PORT) || 10101;

var contentBase = '';
var srcBase = path.join(__dirname, 'src');
var distBase = path.join(__dirname, 'dist');

var config = {

  entry: {
    app: [path.join(srcBase, 'app', 'app.js')],
    vendor: ['angular', 'angular-animate', 'angular-aria', 'angular-messages', 'angular-material', 'angular-material-data-table', 'angular-ui-router']
  },
  output: {
    path: path.join(srcBase, 'app'),
    filename: '[name].bundle.js',
    sourceMapFile: '[file].map'
  },

  plugins: [
    new webpack.optimize.DedupePlugin()
  ],

  addPlugin: (plugin) => this.plugins.push(plugin),

  devTool: env === 'development' ? 'inline-source-map' : 'source-map',

  devServer: {
    historyApiFallback: true,
    contentBase: srcBase,
    stats: {
      colors: true
    },
    port: 8080
  },

  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: 'ng-annotate!babel-loader'},
      {test: /\.json$/, exclude: /node_modules/, loader: 'json'},
      {test: /\.html$/, exclude: /node_modules/, loader: 'raw'},
      {test: /\.less$/, exclude: /node_modules/, loader: 'style!css!less'},
      {test: /\.css$/, exclude: /node_modules/, loader: 'style!css'}
    ]
  }

};

if (env === 'production') {
  config.entry = path.join(distBase, 'app', 'index.js');
  config.output.path = distBase;
  config.addPlugin(new webpack.optimize.UglifyJsPlugin());
}

module.exports = config;
